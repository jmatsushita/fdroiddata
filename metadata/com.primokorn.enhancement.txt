Categories:Theming
License:GPLv3
Web Site:https://gitlab.com/Primokorn/EnhancementTheme
Source Code:https://gitlab.com/Primokorn/EnhancementTheme/tree/HEAD
Issue Tracker:https://gitlab.com/Primokorn/EnhancementTheme/issues

Auto Name:Enhancement
Summary:White theme for CM13
Description:
Enhancement is a theme for custom ROMs having the CM13 theme engine. The goal is
to provide a stock UI with enhanced icons and colors for your device, keeping it
light and clean.
.

Repo Type:git
Repo:https://gitlab.com/Primokorn/EnhancementTheme.git

Build:3.0,3
    commit=f212116518ca91f71cea45103410409823bb27b3
    subdir=theme
    gradle=yes

Build:4.0,4
    commit=f75fcd711ec85c5ec8095d6920376f0cbef83cb5
    subdir=theme
    gradle=yes

Build:5.0,5
    commit=74b4919ee6b725fd98e9da4f3a65e657c157e583
    subdir=theme
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:6.0
Current Version Code:6
