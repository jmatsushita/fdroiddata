AntiFeatures:NonFreeDep
Categories:Internet
License:MIT
Web Site:https://shrimadhavuk.me
Source Code:https://github.com/SpEcHiDe/WhatsAppTransmitter
Issue Tracker:https://github.com/SpEcHiDe/WhatsAppTransmitter/issues

Auto Name:WATransmitter
Summary:Share any file in WhatsApp
Description:
Send any type of file using the popular internet messaging service WhatsApp.
This is achieved by uploading the file to a thirdparty server from where the
user can download and get the file.

WARNING: This app uploads shared files to a foreign server!
.

Repo Type:git
Repo:https://github.com/SpEcHiDe/WhatsAppTransmitter

Build:2.0,2
    commit=33d9c65226cceee7effb33c56b2e267ac1673bb0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0
Current Version Code:2
